//{Includes
//	The platform layer includes a common layer of code shared between game and platform,
//	as well as platform-specific libraries (in this case, windows and d3d11).
#include "SoftBody_common.h"

#define WIN32_LEAN_AND_MEAN 
#include <windows.h>
#include <d3d11_1.h>
#include <d3dcompiler.h>
//}

globally_persistant bool Running;

//{d3d11
struct d3d_state
{
	ID3D11DeviceContext1 *DeviceContext;
	IDXGISwapChain1 *SwapChain;
	ID3D11RenderTargetView *FrameBufferView;
	D3D11_VIEWPORT FrameBufferViewport;
	
	ID3D11VertexShader *DotVertexShader;
	ID3D11PixelShader *DotPixelShader;
	ID3D11Buffer *DotBuffer;
	ID3D11ShaderResourceView *DotBufferView;
	
	ID3D11VertexShader *LineVertexShader;
	ID3D11PixelShader *LinePixelShader;
	ID3D11Buffer *LineBuffer;
	ID3D11ShaderResourceView *LineBufferView;
};

d3d_state InitializeD3D11(HWND Window)
{
	d3d_state Result = d3d_state{};
	
	//{Setup
	D3D_FEATURE_LEVEL FeatureLevels[] = { D3D_FEATURE_LEVEL_11_0 };
	
	ID3D11Device *BaseDevice;
	ID3D11DeviceContext *BaseDeviceContext;
	//TODO: disable the debug information for release builds
	D3D11CreateDevice(0, D3D_DRIVER_TYPE_HARDWARE, 0, D3D11_CREATE_DEVICE_BGRA_SUPPORT | D3D11_CREATE_DEVICE_DEBUG, FeatureLevels, ARRAYSIZE(FeatureLevels), D3D11_SDK_VERSION, &BaseDevice, 0, &BaseDeviceContext);

	ID3D11Device1 *Device;
	BaseDevice->QueryInterface(__uuidof(ID3D11Device1), (void**)(&Device));

	BaseDeviceContext->QueryInterface(__uuidof(ID3D11DeviceContext1), (void**)(&Result.DeviceContext));
	
	IDXGIDevice1 *DXGIDevice;
	Device->QueryInterface(__uuidof(IDXGIDevice1), (void**)(&DXGIDevice));
	
	IDXGIAdapter *DXGIAdapter;
	DXGIDevice->GetAdapter(&DXGIAdapter);
	
	IDXGIFactory2 *DXGIFactory;
	DXGIAdapter->GetParent(__uuidof(IDXGIFactory2), (void**)(&DXGIFactory));
	//}
	
	//{SwapChain
	DXGI_SWAP_CHAIN_DESC1 SwapChainDesc = {
		.Width = 0,
		.Height = 0,
		.Format = DXGI_FORMAT_B8G8R8A8_UNORM,
		.Stereo = false,
		.SampleDesc = { .Count = 1, .Quality = 0 },
		.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT,
		.BufferCount = 2,
		.Scaling = DXGI_SCALING_STRETCH,
		.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD,
		.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED,
		.Flags = 0
	};
	
	DXGIFactory->CreateSwapChainForHwnd(Device, Window, &SwapChainDesc, 0, 0, &Result.SwapChain);
	
	Result.SwapChain->GetDesc1(&SwapChainDesc); //Fill actual window width and height
	
	ID3D11Texture2D *FrameBuffer;
	Result.SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)(&FrameBuffer));
	
	D3D11_RENDER_TARGET_VIEW_DESC RenderTargetViewDesc = {
		.Format = DXGI_FORMAT_B8G8R8A8_UNORM_SRGB,
		.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D
	};
	Device->CreateRenderTargetView(FrameBuffer, &RenderTargetViewDesc, &Result.FrameBufferView);
	//}	
	
	//{Shaders
	ID3DBlob *ErrorBlob;
	//{DotVertex
	/*const char DotVertexShaderCode[] = R"HLSL(
		struct dot
		{
			float2 Position;
			float Radius;
		};
		
		StructuredBuffer<dot> DotBuffer : register(t0);
		
		struct pixel
		{
			float4 xyz : SV_POSITION;
			float2 uv : UV;
		};
		
		pixel vs_main(uint DotID : SV_INSTANCEID, uint VertexID : SV_VERTEXID)
		{
			pixel Result;
			
			dot Dot = DotBuffer[DotID];
			
			float4 Position = float4(Dot.Position, Dot.Position + 2*Dot.Radius);
			
			Result.uv = uint2((VertexID & 1), ((VertexID < 1)||(VertexID > 3)));
			Result.xyz = float4((float2(Position[Result.uv.x*2]/60.0f, Position[(Result.uv.y*2)+1]/33.75)), 0, 1);
			//Result.xyz = float4(Result.uv, 0, 1);
			
			return Result;
		}
	)HLSL";*/
	const char DotVertexShaderCode[] = R"HLSL(
		struct dot
		{
			float4 Color;
			float2 Position;
		};
		
		StructuredBuffer<dot> DotBuffer : register(t0);
		
		struct pixel
		{
			float4 Position : SV_POSITION;
			float4 Color : COLOR;
		};
		
		pixel vs_main(uint DotID : SV_VERTEXID)
		{
			pixel Result;
			
			dot Dot = DotBuffer[DotID];
			
			Result.Position = float4((float2(Dot.Position.x/60.0f, Dot.Position.y/33.75)), 0, 1);
			//Result.Position = float4(Result.uv, 0, 1);
			Result.Color = Dot.Color;
			
			return Result;
		}
	)HLSL";
	ID3DBlob *DotVertexBlob;
	D3DCompile(DotVertexShaderCode, sizeof(DotVertexShaderCode) - 1, 0, 0, 0, "vs_main", "vs_5_0", D3DCOMPILE_DEBUG|D3DCOMPILE_SKIP_OPTIMIZATION, 0, &DotVertexBlob, &ErrorBlob);//D3DCOMPILE_DEBUG|D3DCOMPILE_SKIP_OPTIMIZATION
	if(ErrorBlob) {OutputDebugStringA((char*)ErrorBlob->GetBufferPointer());}
		
	Device->CreateVertexShader(DotVertexBlob->GetBufferPointer(), DotVertexBlob->GetBufferSize(), 0, &Result.DotVertexShader);
	//}
	
	//{DotPixel
	const char DotPixelShaderCode[] = R"HLSL(
		struct pixel
		{
			float4 Position : SV_POSITION;
			float4 Color : COLOR;
		};
			
		float4 ps_main(pixel Pixel) : SV_TARGET
		{
			float4 Color = Pixel.Color;
			
			return Color;
		}
	)HLSL";
	ID3DBlob *DotPixelBlob;
	D3DCompile(DotPixelShaderCode, sizeof(DotPixelShaderCode) - 1, 0, 0, 0, "ps_main", "ps_5_0", D3DCOMPILE_DEBUG|D3DCOMPILE_SKIP_OPTIMIZATION, 0, &DotPixelBlob, &ErrorBlob);
	if(ErrorBlob) {OutputDebugStringA((char*)ErrorBlob->GetBufferPointer());}
		
	Device->CreatePixelShader(DotPixelBlob->GetBufferPointer(), DotPixelBlob->GetBufferSize(), 0, &Result.DotPixelShader);
	//}
	
	//{LineVertex
	const char LineVertexShaderCode[] = R"HLSL(
		struct linestruct
		{
			float4 Color;
			float2 Position1;
			float2 Position2;
		};
		
		StructuredBuffer<linestruct> LineBuffer : register(t0);
		
		struct pixel
		{
			float4 Position : SV_POSITION;
			float4 Color : COLOR;
		};
		
		pixel vs_main(uint LineID : SV_INSTANCEID, uint VertexID : SV_VERTEXID)
		{
			pixel Result;
			
			linestruct Line = LineBuffer[LineID];
			
			if(VertexID){
				Result.Position = float4((float2(Line.Position2.x/60.0f, Line.Position2.y/33.75)), 0, 1);
			}
			else{
				Result.Position = float4((float2(Line.Position1.x/60.0f, Line.Position1.y/33.75)), 0, 1);
			}
			Result.Color = Line.Color;
			
			return Result;
		}
	)HLSL";
	ID3DBlob *LineVertexBlob;
	D3DCompile(LineVertexShaderCode, sizeof(LineVertexShaderCode) - 1, 0, 0, 0, "vs_main", "vs_5_0", D3DCOMPILE_DEBUG|D3DCOMPILE_SKIP_OPTIMIZATION, 0, &LineVertexBlob, &ErrorBlob);//D3DCOMPILE_DEBUG|D3DCOMPILE_SKIP_OPTIMIZATION
	if(ErrorBlob) {OutputDebugStringA((char*)ErrorBlob->GetBufferPointer());}
		
	Device->CreateVertexShader(LineVertexBlob->GetBufferPointer(), LineVertexBlob->GetBufferSize(), 0, &Result.LineVertexShader);
	//}
	
	//{LinePixel
	const char LinePixelShaderCode[] = R"HLSL(
		struct pixel
		{
			float4 Position : SV_POSITION;
			float4 Color : COLOR;
		};
			
		float4 ps_main(pixel Pixel) : SV_TARGET
		{
			float4 Color = Pixel.Color;
			
			return Color;
		}
	)HLSL";
	ID3DBlob *LinePixelBlob;
	D3DCompile(LinePixelShaderCode, sizeof(LinePixelShaderCode) - 1, 0, 0, 0, "ps_main", "ps_5_0", D3DCOMPILE_DEBUG|D3DCOMPILE_SKIP_OPTIMIZATION, 0, &LinePixelBlob, &ErrorBlob);
	if(ErrorBlob) {OutputDebugStringA((char*)ErrorBlob->GetBufferPointer());}
		
	Device->CreatePixelShader(LinePixelBlob->GetBufferPointer(), LinePixelBlob->GetBufferSize(), 0, &Result.LinePixelShader);
	//}
	//}
	
	//{Shader Resources
	#define MAX_DOTS 4096
	D3D11_BUFFER_DESC DotBufferDescription = {
		.ByteWidth = MAX_DOTS * sizeof(render_dot),
		.Usage = D3D11_USAGE_DYNAMIC,
		.BindFlags = D3D11_BIND_SHADER_RESOURCE,
		.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE,
		.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED,
		.StructureByteStride = sizeof(render_dot)
	};
	
	Device->CreateBuffer(&DotBufferDescription, 0, &Result.DotBuffer);
	
	D3D11_SHADER_RESOURCE_VIEW_DESC DotBufferViewDescription = {
		.Format = DXGI_FORMAT_UNKNOWN,
		.ViewDimension = D3D11_SRV_DIMENSION_BUFFER,
		.Buffer = {
			.NumElements = MAX_DOTS
		}
	};
	
	Device->CreateShaderResourceView(Result.DotBuffer, &DotBufferViewDescription, &Result.DotBufferView);
	
	#define MAX_LINES 4096
	D3D11_BUFFER_DESC LineBufferDescription = {
		.ByteWidth = MAX_LINES * sizeof(render_line),
		.Usage = D3D11_USAGE_DYNAMIC,
		.BindFlags = D3D11_BIND_SHADER_RESOURCE,
		.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE,
		.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED,
		.StructureByteStride = sizeof(render_line)
	};
	
	Device->CreateBuffer(&LineBufferDescription, 0, &Result.LineBuffer);
	
	D3D11_SHADER_RESOURCE_VIEW_DESC LineBufferViewDescription = {
		.Format = DXGI_FORMAT_UNKNOWN,
		.ViewDimension = D3D11_SRV_DIMENSION_BUFFER,
		.Buffer = {
			.NumElements = MAX_LINES
		}
	};
	
	Device->CreateShaderResourceView(Result.LineBuffer, &LineBufferViewDescription, &Result.LineBufferView);
	
	Result.FrameBufferViewport =  { 0, 0, static_cast<float>(SwapChainDesc.Width), static_cast<float>(SwapChainDesc.Height), 0, 1 };
	//}
	
	return Result;
}

internal void GameRender(d3d_state *D3D, render_queue *RenderQueue)
{
	float BackgroundColor[4] = {0,0,0,0};
	D3D->DeviceContext->ClearRenderTargetView(D3D->FrameBufferView, BackgroundColor);
	D3D->DeviceContext->RSSetViewports(1, &D3D->FrameBufferViewport);
	D3D->DeviceContext->OMSetRenderTargets(1, &D3D->FrameBufferView, 0);
	
	//{Dots
	D3D->DeviceContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_POINTLIST );
	D3D11_MAPPED_SUBRESOURCE MappedSubresource;
	D3D->DeviceContext->Map(D3D->DotBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &MappedSubresource);
	for(u32 i = 0; i < RenderQueue->DotCount; i++)
	{
		memcpy(MappedSubresource.pData, RenderQueue->Dots, sizeof(render_dot) * RenderQueue->DotCount);
	}
	D3D->DeviceContext->Unmap(D3D->DotBuffer, 0);
	
	D3D->DeviceContext->VSSetShader(D3D->DotVertexShader, 0, 0);
	D3D->DeviceContext->VSSetShaderResources(0, 1, &D3D->DotBufferView);
	D3D->DeviceContext->PSSetShader(D3D->DotPixelShader, 0, 0);
	
	D3D->DeviceContext->Draw(RenderQueue->DotCount,0);
	//}
	
	//{Lines
	D3D->DeviceContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_LINELIST );
	D3D->DeviceContext->Map(D3D->LineBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &MappedSubresource);
	for(u32 i = 0; i < RenderQueue->LineCount; i++)
	{
		memcpy(MappedSubresource.pData, RenderQueue->Lines, sizeof(render_line) * RenderQueue->LineCount);
	}
	D3D->DeviceContext->Unmap(D3D->LineBuffer, 0);
	
	D3D->DeviceContext->VSSetShader(D3D->LineVertexShader, 0, 0);
	D3D->DeviceContext->VSSetShaderResources(0, 1, &D3D->LineBufferView);
	D3D->DeviceContext->PSSetShader(D3D->LinePixelShader, 0, 0);
	
	D3D->DeviceContext->DrawInstanced(2,RenderQueue->LineCount,0,0);
	//}
	
	D3D->SwapChain->Present(1, 0);
}
//}

//Note: Thanks to Raymond Chen for the fullscreen implementation.
internal void ToggleFullscreen(HWND Window)
{
	locally_persistant WINDOWPLACEMENT WindowPosition;
	
	DWORD Style = GetWindowLong(Window, GWL_STYLE);
	if(Style & WS_OVERLAPPEDWINDOW)
	{
		MONITORINFO MonitorInfo = {sizeof(MonitorInfo)};
		if(GetWindowPlacement(Window, &WindowPosition) &&
		   GetMonitorInfo(MonitorFromWindow(Window, MONITOR_DEFAULTTOPRIMARY), &MonitorInfo))
	   {
		   SetWindowLong(Window, GWL_STYLE, Style & ~WS_OVERLAPPEDWINDOW);
		   SetWindowPos(Window, HWND_TOP,
						MonitorInfo.rcMonitor.left, MonitorInfo.rcMonitor.top,
						MonitorInfo.rcMonitor.right - MonitorInfo.rcMonitor.left, 
						MonitorInfo.rcMonitor.bottom - MonitorInfo.rcMonitor.top, 
						SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
	   }
	}
	else
	{
		SetWindowLong(Window, GWL_STYLE, Style | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(Window, &WindowPosition);
		SetWindowPos(Window, 0, 0, 0, 0, 0,
					 SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER |
					 SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
	}
}

LRESULT CALLBACK W32Callback(HWND Window, UINT Message, WPARAM WParam, LPARAM LParam)
{
	switch(Message)
	{
		case WM_DESTROY:
		case WM_CLOSE:
		{
			//TODO(Voran): Recreate Window?
			Running = false;
			return 0;
		} break;
		
		default:
		{
			break;
		};
	}
	
	return DefWindowProc(Window, Message, WParam, LParam);
}

int CALLBACK WinMain(HINSTANCE Instance, HINSTANCE PrevInstance, LPSTR CommandLine, int ShowCode)
{
	//{Window Setup	
	WNDCLASSEX WindowClass = {
		.cbSize = sizeof(WNDCLASSEX),
		.style = CS_HREDRAW|CS_VREDRAW|CS_OWNDC,
		.lpfnWndProc = W32Callback,
		.hInstance = Instance,
		.hIcon = LoadIcon(NULL, IDI_APPLICATION),
		.hCursor = LoadCursor(0, IDC_ARROW),
		.lpszClassName = "SoftBodyWindowClass",
		.hIconSm = LoadIcon(NULL, IDI_APPLICATION)
	};
	ContinueIf(RegisterClassEx(&WindowClass), "Couldn't RegisterClass Window");
	
	HWND Window = CreateWindowEx(
		0,
		WindowClass.lpszClassName,
		"SoftBody",
		WS_OVERLAPPEDWINDOW|WS_VISIBLE,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		0,
		0,
		Instance,
		0
	);
	ContinueIf(Window, "Couldn't create Window");
	
	ToggleFullscreen(Window);
	//}
	
	//{Game Setup
	//TODO: More general file system loading
	HMODULE GameCode = LoadLibrary("A:/SoftBody/build/SoftBody_game.dll");
	ContinueIf(GameCode, "Game could not be loaded");
		
	update *GameUpdate = (update *)GetProcAddress(GameCode, "Update");
	
	game_memory GameMemory = {
		.LastingStorageSize = Megabytes(128),
		.FleetingStorageSize = Gigabytes(1)
	};
	
	GameMemory.LastingStorage = VirtualAlloc(0, GameMemory.LastingStorageSize + GameMemory.FleetingStorageSize,
											 MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
	
	ContinueIf(GameMemory.LastingStorage, "Game Memory could not be initialized");
	GameMemory.FleetingStorage = ((u8 *)GameMemory.LastingStorage + GameMemory.LastingStorageSize);
	//}
	
	d3d_state D3D = InitializeD3D11(Window);
	
	Running = true;
	while(Running)
	{
		MSG Message;
		while(PeekMessage(&Message, 0, 0, 0, PM_REMOVE))
		{
			switch(Message.message)
			{
				case WM_DESTROY:
				{
					//TODO: Ensure things are not CATASTROPHICALLY shut down.
					Running = false;
					return 0;
				} break;
				
				case WM_SYSKEYDOWN:
				case WM_SYSKEYUP:
				case WM_KEYDOWN:
				case WM_KEYUP:
				{
					u32 VKCode = (u32)Message.wParam;
					b32 WasDown = ((Message.lParam & (1 << 30)) != 0);
					b32 IsDown = ((Message.lParam & (1 << 31)) == 0);

					switch(VKCode)
					{
						case VK_ESCAPE:
						{
							if(IsDown && !WasDown)
							{
								Running = false;
							}
						} break;
						
						
						default:
						{
						} break;
					}
				} break;
				
				
				default:
				{
					TranslateMessage(&Message);
					DispatchMessage(&Message);
				} break;
			}
		}
		
		game_input Input = {
			.dt = 1.0f/60.0f //Placeholder for the expected time in a frame, in seconds.
		};
		
		render_queue RenderQueue;
		GameUpdate(&GameMemory, &Input, &RenderQueue);
		GameRender(&D3D, &RenderQueue);
	}
	
	return 0;
}