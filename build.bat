@echo off
REM TODO: Rebuild this for my own needs.
set CompilerFlags= -Oi, -Gm- -GR- -WX -W4 -wd4100 -wd4127 -wd4201 -wd4505 -FC -Z7 -std:c++latest -DINTERNAL_BUILD=1 -DD3D11=1 -nologo
set LinkerFlags= Dwmapi.lib user32.lib gdi32.lib ole32.lib winmm.lib d3d11.lib d3dcompiler.lib -INCREMENTAL:NO

IF NOT EXIST ..\build mkdir ..\build
pushd ..\build

del softbodypdb_*.pdb >nul 2>&1
del softbodypdb_*.raddbg >nul 2>&1
echo WAITING FOR PDB > lock.tmp
cl %CompilerFlags% ..\code\SoftBody_game.cpp -LD /link -PDB:softbodypdb_%random%.pdb /EXPORT:Update -INCREMENTAL:NO
del lock.tmp
cl %CompilerFlags% ..\code\SoftBody_win32.cpp /link %LinkerFlags% 

REM IF "%~1"=="-a" //TODO: flags for building different pieces?

popd