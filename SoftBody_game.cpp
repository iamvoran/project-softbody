//{includes
#include "SoftBody_common.h"
//}

//{Physics
struct shape;
struct pm //Point Mass
{
	v2 P; //Position
	v2 V; //Velocity
	v2 A; //Acceleration
	f32 M;//Mass
	
	shape *Shape;
};

enum constraint_type
{
	CONSTRAINT_NULL,
	
	CONSTRAINT_SPRING_EDGE,
	CONSTRAINT_SPRING,
	
	CONSTRAINT_MAX
};

struct constraint
{
	constraint_type Type;
	
	pm *Point1;
	pm *Point2;
	
	f32 Distance;
	
	constraint *Next;
};

struct shape
{
	u32 ConstraintCount;
	constraint *Constraints;
};

//TODO: Determine better maximums based on the game
#define MAXPOINTCOUNT 10000
#define MAXCONSTRAINTCOUNT 10000
#define MAXSHAPECOUNT 1000
struct world
{
	u32 PointCount;
	pm Points[MAXPOINTCOUNT];
	
	u32 ConstraintCount;
	constraint Constraints[MAXCONSTRAINTCOUNT];
	
	u32 ShapeCount;
	shape Shapes[MAXSHAPECOUNT];
};

inline pm *CreatePointMass(world *World, v2 Position, f32 Mass, shape *Shape, v2 Velocity = v2{0,0})
{
	pm *Result = World->Points + World->PointCount++;
	Result->P = Position;
	Result->V = Velocity;
	Result->A = v2{0,0};
	Result->M = Mass;
	Result->Shape = Shape;
	
	return Result;
}

inline constraint *CreateConstraint(world *World, constraint_type Type, pm *Point1, pm *Point2, f32 Distance)
{
	constraint *Result = World->Constraints + World->ConstraintCount++;
	Result->Type = Type;
	Result->Point1 = Point1;
	Result->Point2 = Point2;
	Result->Distance = Distance;
	Result->Next = 0;
	
	return Result;
}

internal shape *CreateSquare(world *World, v2 Position, f32 Radius, f32 VertexMass = 0.0f, v2 Velocity = v2{0,0})
{
	shape *Result = World->Shapes + World->ShapeCount++;
	Result->ConstraintCount = 0;
	
	pm *Points[4] = {
		CreatePointMass(World, Position + v2{Radius, Radius}, VertexMass, Result, Velocity),
		CreatePointMass(World, Position + v2{Radius, -Radius}, VertexMass, Result, Velocity),
		CreatePointMass(World, Position + v2{-Radius, -Radius}, VertexMass, Result, Velocity),
		CreatePointMass(World, Position + v2{-Radius, Radius}, VertexMass, Result, Velocity)
	};
	
	//TODO: Dynamic max constraint count?
	Assert(World->ConstraintCount+6 < 10000);
	//TODO: Put this in a loop?
	f32 Size = Radius*2;
	Result->Constraints = CreateConstraint(World, CONSTRAINT_SPRING_EDGE, Points[0], Points[1], Size);
	
	constraint *CurrentConstraint = Result->Constraints;
	CurrentConstraint->Next = CreateConstraint(World, CONSTRAINT_SPRING_EDGE, Points[1], Points[2], Size);
	
	CurrentConstraint = CurrentConstraint->Next;
	CurrentConstraint->Next = CreateConstraint(World, CONSTRAINT_SPRING_EDGE, Points[2], Points[3], Size);
	
	CurrentConstraint = CurrentConstraint->Next;
	CurrentConstraint->Next = CreateConstraint(World, CONSTRAINT_SPRING_EDGE, Points[3], Points[0], Size);
	
	CurrentConstraint = CurrentConstraint->Next;
	CurrentConstraint->Next = CreateConstraint(World, CONSTRAINT_SPRING, Points[0], Points[2], Length(v2{Size,Size}));
	
	CurrentConstraint = CurrentConstraint->Next;
	CurrentConstraint->Next = CreateConstraint(World, CONSTRAINT_SPRING, Points[1], Points[3], Length(v2{Size,Size}));
	
	Result->ConstraintCount = 6;
	
	return Result;
}

internal shape *CreateCircle(world *World, v2 Position, f32 Radius, u32 PointCount, f32 VertexMass = 0.0f, v2 Velocity  = v2{0,0})
{
	shape *Result = World->Shapes + World->ShapeCount++;
	Result->ConstraintCount = 0;
	
	Assert(World->PointCount + PointCount < MAXPOINTCOUNT);
	pm *FirstPoint = CreatePointMass(World, Position + v2{Radius,0}, VertexMass, Result, Velocity);
	for(u32 PointIndex = 1; PointIndex < PointCount; PointIndex++)
	{
		//Note: This is negated so that we go clockwise around the circle, because our normals expect clockwise edges.
		f32 DistanceAroundCircle = -((f32)PointIndex / (f32)PointCount);
		CreatePointMass(World, Position + v2{Cosine(TAU*DistanceAroundCircle)*Radius,Sine(TAU*DistanceAroundCircle)*Radius}, VertexMass, Result, Velocity);
	}
	
	Result->Constraints = CreateConstraint(World, CONSTRAINT_SPRING_EDGE, FirstPoint + 0, FirstPoint + 1, Length(FirstPoint[1].P - FirstPoint[0].P));
	Result->ConstraintCount++;
	constraint *CurrentConstraint = Result->Constraints;
	for(u32 ConstraintIndex = 1; ConstraintIndex < PointCount; ConstraintIndex++)
	{
		pm *Point1 = FirstPoint + ConstraintIndex;
		pm *Point2 = FirstPoint + (ConstraintIndex+1)%PointCount;
		CurrentConstraint->Next = CreateConstraint(World, CONSTRAINT_SPRING_EDGE, Point1, Point2, Length(Point2->P - Point1->P));
		CurrentConstraint = CurrentConstraint->Next;
		Result->ConstraintCount++;
		
		pm *Point3 = FirstPoint + (ConstraintIndex+2)%PointCount;
		CurrentConstraint->Next = CreateConstraint(World, CONSTRAINT_SPRING, Point1, Point3, Length(Point3->P - Point1->P));
		CurrentConstraint = CurrentConstraint->Next;
		Result->ConstraintCount++;
		
		pm *Point4 = FirstPoint + (ConstraintIndex+4)%PointCount;
		CurrentConstraint->Next = CreateConstraint(World, CONSTRAINT_SPRING, Point1, Point4, Length(Point4->P - Point1->P));
		CurrentConstraint = CurrentConstraint->Next;
		Result->ConstraintCount++;
	}
	
	return Result;
}

inline v2 ClosestPoint(v2 A, v2 B, v2 C)
{
	v2 LineVector = C - B;
	f32 T = Inner(A - B, LineVector) / Inner(LineVector, LineVector);
	v2 Result = B + (LineVector)*T;
	
	return Result;
}

inline v2 ClosestPoint(pm *Point, pm *LinePoint1, pm *LinePoint2)
{
	v2 Result = ClosestPoint(Point->P, LinePoint1->P, LinePoint2->P);
	
	return Result;
}

f32 DistanceToSide(v2 A, v2 B, v2 C)
{
	v2 DistanceToB = A - B;
	v2 DistanceToC = A - C;
	v2 LineBC = C - B;
	
	f32 ProjectedDistanceToBSquared = Inner(DistanceToB, LineBC);
	f32 LineBCLengthSquared = Inner(LineBC, LineBC);
	
	f32 Result = Inner(DistanceToB, DistanceToB) - ProjectedDistanceToBSquared*ProjectedDistanceToBSquared/LineBCLengthSquared;
	
	return Result;
}

f32 DistanceToSide(v2 Point, pm *LinePoint1, pm *LinePoint2)
{
	f32 Result = DistanceToSide(Point, LinePoint1->P, LinePoint2->P);
	
	return Result;
}
//}

//{Memory Zone
//Note: This is often called an arena-based memory allocation strategy.
struct memory_zone
{
	size_t Size;
	size_t Used;
	u32 TemporaryMemoryCount;
	u8 *Base;
};

#define FillZone(Zone, type) (type *)FillZone_(Zone, sizeof(type))
#define FillZoneArray(Zone, count, type) (type *)FillZone_(Zone, (count) * sizeof(type))
#define FillZoneSize(Zone, Size) FillZone_(Zone, Size)
inline void *FillZone_(memory_zone *Zone, size_t Size)
{
	Assert(Size);
	Assert((Zone->Used + Size) <= Zone->Size);
	void *Result = Zone->Base + Zone->Used;
	Zone->Used += Size;
	
	return Result;
}

internal void InitializeZone(memory_zone *Zone, size_t Size, void *Base)
{
	Zone->Size = Size;
	Zone->Used = 0;
	Zone->Base = reinterpret_cast<u8 *>(Base);
}
//}

struct lasting_state
{
	memory_zone WorldZone; //Holds all our shapes in memory
	world *World;
	
	shape *TestShape;
	shape *TestShape2;
};

struct fleeting_state
{
	memory_zone Zone;
	b32 Initialized;
};

//{Simulation Functions
b32 MovePoint(world *World, pm *Point, f32 dt)
{
	//Note: Drag to slow things down, since we're not in space right now.
	//		The magic number chosen here is arbitrary, and would be tuned later.
	Point->A += Point->V*-1.0f;
	
	//Note: Apply gravity. The magic number chosen here is arbitrary, and would be tuned later.
	if(Point->M < INFINITY)
	{
		Point->A.Y -= 9.1f*Point->M;
	}
	
	if(!Point->A)
	{
		return false; //Note: We're not moving anywhere.
	}
	
	//TODO: Use positive infinity mass for immovable objects
	if(Point->M == INFINITY){ Point->A = v2{0,0}; Point->V = v2{0,0};}
	
	v2 ChangeInPosition = 0.5f*Point->A*Square(dt) + Point->V*dt;
	v2 NewPosition = Point->P + ChangeInPosition;
	Point->V += Point->A*dt;
	
	b32 Collides = false;
	//TODO: This is just a crude way of colliding this shape with another as a stopgap,
	//		until we get around to better managing which shapes we test against.
	for(u32 ShapeIndex = 0; ShapeIndex < World->ShapeCount; ShapeIndex++)
	{
		shape *Shape = World->Shapes + ShapeIndex;
		if(Shape == Point->Shape) continue;
		
		v2 Normal = v2{0,0};
		//Note: We don't need to be testing anything this far anyways.
		//		We can set this based on spatial partitioning instead later.
		f32 LowestDistanceSquared = 1000; 
		pm *CollisionPoint1 = 0;
		pm *CollisionPoint2 = 0;
		
		constraint *Constraint = Shape->Constraints;
		while(Constraint)
		{
			if(Constraint->Type == CONSTRAINT_SPRING_EDGE)
			{
				pm *Point1 = Constraint->Point1;
				pm *Point2 = Constraint->Point2;
				
				if(NewPosition.Y >= Minimum(Point1->P.Y, Point2->P.Y))
				{
					if(NewPosition.Y < Maximum(Point1->P.Y, Point2->P.Y))
					{
						if(NewPosition.X < Maximum(Point1->P.X, Point2->P.X))
						{
							//Note: This finds a point on the line between Point1 and Point2,
							//		at the same Y as our original point.
							f32 Intersection = (NewPosition.Y - Point1->P.Y) * (Point2->P.X - Point1->P.X) / 
											   (Point2->P.Y - Point1->P.Y) + Point1->P.X;
							
							if(Point1->P.X == Point2->P.X || NewPosition.X <= Intersection)
							{
								Collides = !Collides;
							}
						}						
					}
				}
				
				v2 ThisNormal = v2{-(Point2->P.Y - Point1->P.Y), (Point2->P.X - Point1->P.X)};
				f32 ThisDistanceSquared = DistanceToSide(NewPosition, Point1, Point2);
				if(ThisDistanceSquared < LowestDistanceSquared)
				{
					LowestDistanceSquared = ThisDistanceSquared;
					Normal = ThisNormal / Length(ThisNormal);
					CollisionPoint1 = Point1;
					CollisionPoint2 = Point2;
				}
			}
			Constraint = Constraint->Next;
		}
		
		if(Collides)
		{
			//TODO: better collision response, based on mass
			f32 DistanceToMove = SquareRoot(LowestDistanceSquared+0.01f);
			
			Assert(CollisionPoint1);
			Assert(CollisionPoint2);
			f32 TotalMass = Point->M + CollisionPoint1->M + CollisionPoint2->M;
			if(CollisionPoint1->M < INFINITY)
			{
				Assert(TotalMass);
				f32 ThisPointPortion = TotalMass - CollisionPoint1->M / TotalMass;
				CollisionPoint1->P -= Normal*DistanceToMove*ThisPointPortion;
			}
			else{ TotalMass = Point->M + CollisionPoint2->M;}
			if(CollisionPoint2->M < INFINITY)
			{
				Assert(TotalMass);
				f32 ThisPointPortion = TotalMass - CollisionPoint1->M / TotalMass;
				CollisionPoint2->P -= Normal*DistanceToMove*ThisPointPortion;
			}
			else{ TotalMass = Point->M + (CollisionPoint1->M < INFINITY? CollisionPoint1->M:0);}
			
			Assert(TotalMass);
			f32 ThisPointPortion = TotalMass - Point->M / TotalMass;
			NewPosition += Normal*DistanceToMove*ThisPointPortion;
			Point->V -= Inner(Point->V, Normal)*Normal;
		}
	}
	
	Point->P = NewPosition;
	Point->A = v2{0,0};
	
	return Collides;
}
//}

extern "C" UPDATE(Update)
{
	lasting_state *LastingState = (lasting_state *)Memory->LastingStorage;
	if(!Memory->Initialized)
	{
		InitializeZone(&LastingState->WorldZone, Memory->LastingStorageSize - sizeof(lasting_state), (u8 *)Memory->LastingStorage + sizeof(lasting_state));
		LastingState->World = FillZone(&LastingState->WorldZone, world);
		
		RenderQueue->DotCount = 0;
		RenderQueue->MaxDotCount = 0;
		RenderQueue->Dots = 0;
		RenderQueue->LineCount = 0;
		RenderQueue->MaxLineCount = 0;
		RenderQueue->Lines = 0;
		
		//LastingState->TestShape = CreateSquare(LastingState->World, v2{0, 0}, 0.5f, 1.0f);
		LastingState->TestShape = CreateCircle(LastingState->World, v2{0, 40}, 10.0f, 16, 1.0f);
		//LastingState->TestShape2 = CreateSquare(LastingState->World, v2{0, -20}, 16.0f, INFINITY);
		LastingState->TestShape2 = CreateCircle(LastingState->World, v2{0, -10}, 16.0f, 16, 0.1f);
			
		Memory->Initialized = true;
	}
	
	Assert(sizeof(fleeting_state) <= Memory->FleetingStorageSize);
	fleeting_state *FleetingState = (fleeting_state *)Memory->FleetingStorage;
	if(!FleetingState->Initialized)
	{
		InitializeZone(&FleetingState->Zone, Memory->FleetingStorageSize - sizeof(fleeting_state), (u8 *)Memory->FleetingStorage + sizeof(fleeting_state));
		
		RenderQueue->MaxDotCount = Megabytes(2)/sizeof(render_dot);
		RenderQueue->Dots = FillZoneArray(&FleetingState->Zone, RenderQueue->MaxDotCount, render_dot);
		RenderQueue->MaxLineCount = Megabytes(4)/sizeof(render_line);
		RenderQueue->Lines = FillZoneArray(&FleetingState->Zone, RenderQueue->MaxLineCount, render_line);
		
		FleetingState->Initialized = true;
	}
	
	RenderQueue->DotCount = 0;
	RenderQueue->LineCount = 0;
	
	//TODO: shape-based sparsity and updating
	
	for(u32 ShapeIndex = 0;ShapeIndex < LastingState->World->ShapeCount; ShapeIndex++)
	{
		shape *Shape = LastingState->World->Shapes + ShapeIndex;
		for(u32 ConstraintIndex = 0; ConstraintIndex < Shape->ConstraintCount; ConstraintIndex++)
		{
			constraint *Constraint = Shape->Constraints + ConstraintIndex;
			switch(Constraint->Type)
			{
				case CONSTRAINT_SPRING_EDGE:
				case CONSTRAINT_SPRING:
				{
					v2 Spring = Constraint->Point2->P - Constraint->Point1->P;
					f32 SpringDistance = Length(Spring);
					v2 SpringDirection = Spring / SpringDistance;
					
					f32 DistanceDifference = Constraint->Distance - SpringDistance;
					v2 RelativeVelocity = Constraint->Point2->V - Constraint->Point1->V;
					f32 RelativeVelocityAlongSpring = Inner(RelativeVelocity, Spring);
					
					f32 Stiffness = 100.0f;
					f32 Damping = 0.1f;
					v2 Force = SpringDirection *((DistanceDifference * Stiffness) - (RelativeVelocityAlongSpring * Damping));

					//TODO: Stop using dumb methods of selecting shapes, and use spatial partitioning instead
					Constraint->Point1->A -= Force;
					Constraint->Point2->A += Force;
					
					RenderQueue->Lines[RenderQueue->LineCount++] = render_line{
						.Color = v4{.R = (f32)(Constraint->Type == CONSTRAINT_SPRING_EDGE)*0.5f+0.5f, .G = (f32)(DistanceDifference > 0), .B = (f32)(DistanceDifference < 0), .A = 1.0f},
						.Position1 = Constraint->Point1->P,
						.Position2 = Constraint->Point2->P
					};
				} break;
				
				InvalidDefaultCase;
			}
		}
	}
	
	for(u32 PointIndex = 0; PointIndex < LastingState->World->PointCount; PointIndex++)
	{
		pm *Point = LastingState->World->Points + PointIndex;
		
		//TODO: Select shapes in a way that isn't dumb
		//b32 Collides = MovePoint(Point, Input->dt, &LastingState->World->Shapes[
		//						 PointIndex/(LastingState->World->PointCount/LastingState->World->ShapeCount)]);
		b32 Collides = MovePoint(LastingState->World, Point, Input->dt);
		RenderQueue->Dots[RenderQueue->DotCount++] = render_dot{
			.Color = v4{.R = 1.0f, .G = 0.0f, .B = (f32)Collides, .A = 1.0f},
			.Position = Point->P
		};
	}
		
	//MoveAndRenderShape(LastingState->TestShape, RenderQueue, v2{0,0}, Input->dt, LastingState->TestShape2);
	//MoveAndRenderShape(LastingState->TestShape2, RenderQueue, v2{0,0}, Input->dt, LastingState->TestShape);
}