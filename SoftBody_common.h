//{Common Definitions
//	These defines make the code easier to read.
//	They also save time typing, when writing a lot of the same boilerplate.
#define internal static
#define globally_persistant static
#define locally_persistant static

#define ArrayCount(arr) (sizeof((arr)) / (sizeof((arr)[0])))
#define Minimum(A, B) ((A < B) ? A : B)
#define Maximum(A, B) ((A > B) ? A : B)

#define Kilobytes(Value) ((Value)*1024LL)
#define Megabytes(Value) (Kilobytes(Value)*1024LL)
#define Gigabytes(Value) (Megabytes(Value)*1024LL)
#define Terabytes(Value) (Gigabytes(Value)*1024LL)

#include <stdint.h>
#include <math.h>

#define i8  int8_t
#define i16 int16_t
#define i32 int32_t
#define i64 int64_t

#define u8  uint8_t
#define u16 uint16_t
#define u32 uint32_t
#define u64 uint64_t

#define b32 uint32_t
#define f32 float
#define f64 double

#define Assert(Test) if(!(Test)) {*(int *)0 = 0;} 
#define ContinueIf(Test,ErrorMessage) Assert(Test);//TODO: Logging
#define InvalidCodePath Assert(!"InvalidCodePath");
#define InvalidDefaultCase default:{InvalidCodePath}break;
//}

//{Math
#define PI 3.1415927f
#define TAU (PI*2)
struct v2
{
	f32 X, Y;
};

union v4
{
	struct
	{
		f32 X, Y, Z, W;
	};
	struct
	{
		f32 R, G, B, A;
	};
};

inline v2 operator+(v2 A, v2 B)
{
	v2 Result = {
		.X = A.X + B.X,
		.Y = A.Y + B.Y,
	};
	return Result;
}

inline v2 operator+(v2 A, f32 B)
{
	v2 Result = {
		.X = A.X + B,
		.Y = A.Y + B,
	};
	return Result;
}

inline v2 operator-(v2 A)
{
	v2 Result = {
		.X = 0-A.X,
		.Y = 0-A.Y
	};
	
	return Result;
}

inline v2 operator-(v2 A, v2 B)
{
	v2 Result = {
		.X = A.X - B.X,
		.Y = A.Y - B.Y
	};
	
	return Result;
}

inline v2 operator-(v2 A, f32 B)
{
	v2 Result;
	
	Result.X = A.X - B;
	Result.Y = A.Y - B;
	
	return Result;
}

inline v2 operator*(v2 A, v2 B)
{
	v2 Result = {
		.X = A.X * B.X,
		.Y = A.Y * B.Y
	};
	
	return Result;
}

inline v2 operator*(v2 A, f32 B)
{
	v2 Result;
	
	Result.X = A.X * B;
	Result.Y = A.Y * B;
	
	return Result;
}

inline v2 operator*(f32 A, v2 B)
{
	v2 Result = B * A;
	
	return Result;
}

inline v2 operator/(v2 A, f32 B)
{
	v2 Result;
	
	Result.X = A.X / B;
	Result.Y = A.Y / B;
	
	return Result;	
}

inline v2 &operator+=(v2 &A, v2 B)
{
	A = A + B;
	
	return A;
}

inline v2 &operator+=(v2 &A, f32 B)
{
	A = A + B;
	
	return A;
}

inline v2 &operator-=(v2 &A, v2 B)
{
	A = A - B;
	
	return A;
}

inline v2 &operator*=(v2 &A, v2 B)
{
	A = A * B;
	
	return A;
}

inline v2 &operator*=(v2 &A, f32 B)
{
	A = A * B;
	
	return A;
}

inline b32 operator==(v2 A, v2 B)
{
	bool Result = ((A.X == B.X) && 
				   (A.Y == B.Y));
	return Result;
}

inline b32 operator!(v2 A)
{
	bool Result = !(A.X || A.Y);
	
	return Result;
}

inline f32 Square(f32 A)
{
	f32 Result = A*A;
	
	return Result;
}

inline f32 Inner(v2 A, v2 B)
{
	f32 Result = A.X*B.X + A.Y*B.Y;
	
	return Result;
}

inline f32 SquareRoot(f32 Float)
{
	f32 Result = sqrtf(Float);
	
	return Result;
}

inline f32 Cosine(f32 Float)
{
	f32 Result = cosf(Float);
	
	return Result;
}

inline f32 Sine(f32 Float)
{
	f32 Result = sinf(Float);
	
	return Result;
}

inline f32 LengthSquared(v2 A)
{
	f32 Result = Inner(A, A);
	
	return Result;
}

inline f32 Length(v2 A)
{
	f32 Result = SquareRoot(LengthSquared(A));
	
	return Result;
}
//}

//{Rendering
struct render_dot
{
	v4 Color;
	v2 Position;
};

struct render_line
{
	v4 Color;
	v2 Position1;
	v2 Position2;
};

struct render_queue
{
	u32 DotCount;
	u32 MaxDotCount;
	render_dot *Dots;
	
	u32 LineCount;
	u32 MaxLineCount;
	render_line *Lines;
};
//}

struct game_input
{
	f32 dt;
	
	//TODO: Actual input
};

struct game_memory
{
	b32 Initialized;
	
	u64 LastingStorageSize;
	u64 FleetingStorageSize;
	
	void *LastingStorage;
	void *FleetingStorage;
};

#define UPDATE(name) void name(game_memory *Memory, game_input *Input, render_queue *RenderQueue)
typedef UPDATE(update);
